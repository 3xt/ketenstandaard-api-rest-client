
### variables from .vscode/settings.json

###
# Ketenstandaard also offers a proxy to the ETIM Api
# (for a complete overview of the ETIM Api: https://etimapi.etim-international.com/swagger/index.html)
#
# For using the ETIM Api via the Ketenstandaard proxy you need:
# - Ketenstandaard client_id/client_secret with permission for the Api and ETIM scope
# - "authUrl": "https://authorize.ketenstandaard.nl"
# - "baseUrl": "https://api.ketenstandaard.nl"
#
###

###
# @name ClientCredentialsGrant
POST {{authUrl}}/connect/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded

grant_type=client_credentials
&client_id={{client_id}}
&client_secret={{client_secret}}
&scope=Api ETIM

###
# Return all languages the client is allowed to
GET {{baseUrl}}/ETIM/api/v2.0/Misc/LanguagesAllowed HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

###
# Return all releases
GET {{baseUrl}}/ETIM/api/v2.0/Misc/Releases HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

###
# Return all classes with description/synonyms in nl-NL
# with 10 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Class/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 10,
	"Languagecode": "nl-NL",
	"Include": {
		"Descriptions": true
	}
}

###
# Return all classes including Group field for a specific Release within some Groups
# with 20 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Class/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 20,
	"Languagecode": "nl-NL",
	"Filters": [
		{
			"Code": "Release",
			"Values": ["ETIM-9.0"]
		},
		{
			"Code": "Group",
			"Values": ["EG000017", "EG000020"]
		}
	],
	"Include": {
		"Fields": ["Group"],
		"Descriptions": false,
		"Translations": false
	}
}

###
# Return a specific class with maximum details
# - Group, Releases, Features
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/Class/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EC002883",
	"version": 3,
	"Languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Fields": ["Group", "Releases", "Features"]
	}
}

###
# Return a specific class based on Release
POST {{baseUrl}}/ETIM/api/v2.0/Class/DetailsForRelease HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EC002883",
	"release": "DYNAMIC",
	"Languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true,
		"Fields": ["Group", "Releases", "Features"]
	}
}

###
# Return all features with minimal details
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Feature/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL"
}

###
# Return all local features for the Netherlands including descriptions
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Feature/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL",
	"Deprecated": false,
	"Local": true,
	"LocalStandardCountry": "NL",
	"Include": { 
		"Descriptions": true
	}
}

###
# Return a specific feature with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
# - FeatureGroup field
POST {{baseUrl}}/ETIM/api/v2.0/Feature/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EF000004",
	"languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true,
		"Fields": ["FeatureGroup"]
	}
}

###
# Return all featuregroups where description meets a specific searchstring
# - Descriptions in EN language
# with 20 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/FeatureGroup/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 20,
	"Languagecode": "EN",
	"SearchString": "Other",
	"Include": { 
		"Descriptions": true
	}
}

###
# Return a specific featuregroup with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/FeatureGroup/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EFG00004",
	"languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true
	}
}

###
# Return all available groups within ETIM-9.0 Release
# including nl-NL Descriptions
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Group/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true
	},
	"Filters": [
		{
			"Code": "Release",
			"Values": ["ETIM-9.0"]
		}
	]
}

###
# Return a specific group with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/Group/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EG020005",
	"languagecode": "nl-NL",
	"Include": { 
		"Fields": ["Releases"],
		"Descriptions": true,
		"Translations": true
	}
}

###
# Return all classes where description meets a specific searchstring
# - Descriptions in nl-NL language
# with 20 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/ModellingClass/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 20,
	"Languagecode": "nl-NL",
	"SearchString": "Aansluiting",
	"Include": {
		"Descriptions": true
	}
}

###
# Return a specific class with minimal details
POST {{baseUrl}}/ETIM/api/v2.0/ModellingClass/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "MC000013",
	"version": 3,
	"languagecode": "EN"
}

###
# Return all groups with minimal details
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/ModellingGroup/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL"
}

###
# Return a specific group with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/ModellingGroup/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "MG000001",
	"languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true
	}
}

###
# Return all units with minimal details
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Unit/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL"
}

###
# Return a specific value with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/Unit/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EU571097",
	"languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true
	}
}

###
# Return all values with minimal details
# with 100 items per request starting at 0
POST {{baseUrl}}/ETIM/api/v2.0/Value/Search HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"From": 0,
	"Size": 100,
	"Languagecode": "nl-NL"
}

###
# Return a specific value with maximum details
# - Descriptions in nl-NL language
# - Translations for all languages the client is entitled to
POST {{baseUrl}}/ETIM/api/v2.0/Value/Details HTTP/1.1
Content-Type: application/json
Authorization: Bearer {{ClientCredentialsGrant.response.body.$.access_token}}

{
	"code": "EV000397",
	"languagecode": "nl-NL",
	"Include": { 
		"Descriptions": true,
		"Translations": true
	}
}